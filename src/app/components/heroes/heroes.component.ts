import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { HeroesService } from '../../services/heroes.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})

export class HeroesComponent implements OnInit {

  heroesTitle: string[] = ['id', 'superhero', 'publisher', 'alterEgo', 'firstAppearance', 'characters', 'image'];
  heroes = new MatTableDataSource();
  status: number;
  loading: boolean = true;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private heroService: HeroesService) { }

  ngOnInit(): void {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(data => {
        this.heroes = new MatTableDataSource();
        this.heroes.data = data.payload;
        this.heroes.paginator = this.paginator
        this.heroes.sort = this.sort;
        this.status = data.status;
        setTimeout(() => {
          this.loading = false;
        }, 2000);
      }, error => {
        console.log(error);
        setTimeout(() => {
          this.loading = false;
        }, 2000);
      });
  }

}
