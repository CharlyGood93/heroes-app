import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class HeroesService {

  private url: string = environment.apiURL;

  constructor(private http: HttpClient) { }

  getHeroes(): Observable<any> {
    return this.http.get(this.url);
  }

}
